# used for debugging and connecting to DB server
# from bastion host
output "db_host" {
  value = aws_db_instance.main.address
}

# bastion host address
output "bastion_host" {
  value = aws_instance.bastion.public_dns
}

# Load Balancer DNS Name
output "loadbalancer_endpoint" {
  value = aws_lb.api.dns_name
}

# API Endpoint
output "api_endpoint" {
  value = aws_route53_record.app.fqdn
}


