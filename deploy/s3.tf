##############################
# S3 Bucket for File Uploads #
##############################
resource "aws_s3_bucket" "app_public_files" {
  bucket = "${local.prefix}-files-yurlu46lpvrazrpsi8sc"
  acl    = "public-read"
  # simplify destroying bucket
  force_destroy = true
}
