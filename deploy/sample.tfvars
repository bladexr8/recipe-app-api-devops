# used when developing locally
# as sample default values
# actual production values should
# be set up in a CI tool so they are
# not accessible in source code
db_username = "recipeapp"
db_password = "changeme"

# django app secret key
django_secret_key = "changeme"
