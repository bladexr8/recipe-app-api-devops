# Your VPC must have at least two subnets. These subnets must be in two 
# different Availability Zones # in the AWS Region where you want to 
# deploy your DB instance. A subnet is a segment of a VPC's IP
# address range that you can specify and that lets you group 
# instances based on your security and operational needs.

# If you want your DB instance in the VPC to be publicly accessible, 
# you must enable the VPC attributes DNS hostnames and DNS resolution

# The CIDR blocks in each of your subnets must be large enough to 
# accommodate spare IP addresses for Amazon RDS to use during maintenance activities, 
# including failover and compute scaling.

# Your VPC must have a DB subnet group that you create. You create a DB subnet 
# group by specifying the subnets you created. Amazon RDS uses that DB subnet 
# group and your preferred Availability Zone to choose a subnet and an IP address 
# within that subnet to assign to your DB instance.
resource "aws_db_subnet_group" "main" {
  name = "${local.prefix}-main"
  subnet_ids = [
    aws_subnet.private_a.id,
    aws_subnet.private_b.id
  ]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}

# Your VPC must have a VPC security group that allows access to the DB instance.
resource "aws_security_group" "rds" {
  description = "Allow access to the RDS database instance."
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # only allow access from bastion host
    # and ECS Service
    security_groups = [
      aws_security_group.bastion.id,
      aws_security_group.ecs_service.id
    ]
  }

  tags = local.common_tags
}

# Postgres RDS Instance
# Note this is attached to the subnet group, not individual
# subnets themselves, and the rds security group created
# earlier

# allocated_storage, storage_type - storage in GB and EBS SSD type
# backup_retention_period - number of days to retain backups

# multi_az - determine if DB runs in multiple AZ's, should be "true" for 
#            a real production app for high availability

# skip_final_snapshot - flag to determine if AWS takes a backup of DB when it is destroyed
resource "aws_db_instance" "main" {
  identifier              = "${local.prefix}-db"
  name                    = "recipe"
  allocated_storage       = 20
  storage_type            = "gp2"
  engine                  = "postgres"
  engine_version          = "11.4"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.main.name
  password                = var.db_password
  username                = var.db_username
  backup_retention_period = 0
  multi_az                = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds.id]

  tags = merge(
    local.common_tags,
    map("Name", "${local.prefix}-main")
  )
}
